# Some inspiration taken from https://github.com/trastle/docker-webdl

FROM python:3.12-bookworm

WORKDIR /usr/src/app

RUN apt-get update -y \
    && apt-get --no-install-recommends -y install \
        ffmpeg \
        streamlink \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt
COPY *.py LICENSE README.md /usr/src/app/

# Webdl does not like running as root so setup a user & home dir
RUN useradd -ms /bin/bash webdl -d /home/webdl && \
    mkdir -p /home/webdl/data

RUN chown -R webdl:webdl /home/webdl
USER webdl
WORKDIR /home/webdl/data

CMD ["python", "/usr/src/app/autograbber.py", "/home/webdl/data"]
